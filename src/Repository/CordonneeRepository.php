<?php

namespace App\Repository;

use App\Entity\Cordonnee;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Cordonnee|null find($id, $lockMode = null, $lockVersion = null)
 * @method Cordonnee|null findOneBy(array $criteria, array $orderBy = null)
 * @method Cordonnee[]    findAll()
 * @method Cordonnee[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CordonneeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Cordonnee::class);
    }

    // /**
    //  * @return Cordonnee[] Returns an array of Cordonnee objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Cordonnee
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
