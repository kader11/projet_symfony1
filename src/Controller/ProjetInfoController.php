<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

use App\Entity\User;
use App\Entity\ProjetInformatique;
use App\Entity\Hobbie;
use App\Entity\Cordonnee;
class ProjetInfoController extends AbstractController
{
    /**
     * @Route("/projet/info", name="projet_info")
     */
    public function index()
    {  
        $entityManager = $this->getDoctrine()->getManager();
        $userRepository = $entityManager->getRepository(User::class);
        $users = $userRepository->findAll();
        
 
     
    // if (empty($User))  {
          /*  
            $user1=new User();
            $user1->setNom('yousfi');
            $user1->setPrenom('kader');
            $user1->setTechnoFavoris(['symfony','angular']);
            $user1->setlangueFavoris(['Php','javascript']);
            $entityManager->persist($user1);
            
            ////////////////////////////
            $projet1=new ProjetInformatique();
            $projet1->setNom('angular');
            $projet1->setLien ('https://github.com/kader21/angular1');
            $projet1->setUser($user1);
            $entityManager->persist($projet1);


            $projet2=new ProjetInformatique();
            $projet2->setNom('symfony');
            $projet2->setLien ('https://github.com/kader21/TP02');
            $projet2->setUser($user1);
            $entityManager->persist($projet2);


            $hobbie1=new Hobbie();
            $hobbie1->setNom('Sport');
            $hobbie1->setDescription ('football');
            $hobbie1->setUser($user1);
            $entityManager->persist($hobbie1);


            $hobbie2=new Hobbie();
            $hobbie2->setNom('filme ');
            $hobbie2->setDescription ('action');
            $hobbie2->setUser($user1);
            $entityManager->persist($hobbie2);

            $coordonnee1=new Cordonnee();
            $coordonnee1 ->setEmail("kader3@yahoo.fr");
            $coordonnee1 ->setAdresse("12 rue dakar Montpellier");
            $coordonnee1 ->setTelephone("0700000001");
            $coordonnee1 ->setReseauxSociaux("www.linkedin.com/in/abdelkader-yousfi-61428b197");
            $coordonnee1->setUser($user1);
            $entityManager->persist($coordonnee1);

           
           
            $entityManager->flush();


         //   }else{} 

*/

        return $this->render('projet_info/index.html.twig', [
            'controller_name' => 'ProjetInfoController',
            'users'=> $userRepository->findAll(),
        ]);
    }
}
